import { useEffect, useState } from 'react';
import { getQuestionsList } from '../Api/client';
import { QuestionsList } from '../Api/client.d';

const useQuestionsListData = () => {
  const [questions, setQuestions] = useState<QuestionsList>();
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);

  useEffect(() => {
    const fetchQuestionsList = async () => {
      setLoading(true);
      try {
        const data = await getQuestionsList();
        setQuestions(data);
        setLoading(false);
      } catch {
        setError(true);
      }
    };
    fetchQuestionsList();
  }, []);

  return { questions, error, loading };
};

export default useQuestionsListData;
