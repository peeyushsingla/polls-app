import { renderHook } from '@testing-library/react-hooks';
import useQuestionsListData from './useQuestionsListData';
import { getQuestionsList } from '../Api/client';
import { questionListMock } from '../Api/client.mocks';

jest.mock('../Api/client');
const getQuestionsListMock = getQuestionsList as jest.Mock;

describe('useQuestionsListData', () => {
  it('returns questions data', async () => {
    getQuestionsListMock.mockResolvedValue(questionListMock);
    const { result, waitForValueToChange } = renderHook(() =>
      useQuestionsListData()
    );
    expect(result.current.loading).toBe(true);
    expect(result.current.error).toBe(false);

    await waitForValueToChange(() => result.current.loading);

    expect(result.current.loading).toBe(false);
    expect(result.current.questions).toEqual(questionListMock);
  });
});
