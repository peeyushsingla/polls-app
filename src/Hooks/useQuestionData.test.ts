import { renderHook } from '@testing-library/react-hooks';
import useQuestionData from './useQuestionData';
import { getQuestion } from '../Api/client';
import { questionMock } from '../Api/client.mocks';

jest.mock('../Api/client');
const getQuestionMock = getQuestion as jest.Mock;

describe('useQuestionData', () => {
  it('returns question data', async () => {
    getQuestionMock.mockResolvedValue(questionMock);
    const { result, waitForValueToChange } = renderHook(() =>
      useQuestionData('1')
    );
    expect(result.current.loading).toBe(true)
    expect(result.current.error).toBe(false);

    await waitForValueToChange(() => result.current.loading);

    expect(result.current.loading).toBe(false);
    expect(result.current.question).toEqual(questionMock);
  });
});
