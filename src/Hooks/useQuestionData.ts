import { useEffect, useState } from 'react';
import { getQuestion } from '../Api/client';
import { Question } from '../Api/client.d';

const useQuestionData = (questionId: string) => {
  const [question, setQuestion] = useState<Question>();
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);

  useEffect(() => {
    const fetchQuestion = async () => {
      setLoading(true);
      try {
        const data = await getQuestion(questionId);
        setQuestion(data);
        setLoading(false);
      } catch {
        setError(true);
      }
    };
    fetchQuestion();
  }, [questionId]);

  return { question, error, loading };
};

export default useQuestionData;
