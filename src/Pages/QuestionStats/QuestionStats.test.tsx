import React from 'react';
import QuestionStats from './QuestionStats';
import { render } from '@testing-library/react';
import useQuestionData from '../../Hooks/useQuestionData';
import { questionMock } from '../../Api/client.mocks';

jest.mock('../../Hooks/useQuestionData');
const useQuestionDataMock = useQuestionData as jest.Mock;

jest.mock('react-router-dom', () => ({
  useParams: () => ({ questionId: 'questionId' })
}));

describe(('QuestionStats'), () => {
  afterEach(() => {
    jest.clearAllMocks();
  })

  it('renders question stats', () => {
    useQuestionDataMock.mockReturnValue({
      error: false,
      question: questionMock,
      loading: false
    })
    const { asFragment } = render(<QuestionStats />)
    expect(asFragment()).toMatchSnapshot()
  })

  it('renders loading', () => {
    useQuestionDataMock.mockReturnValue({
      error: false,
      question: undefined,
      loading: true
    })
    const { getByTestId } = render(<QuestionStats />)
    expect(getByTestId('spinner')).toBeInTheDocument()
  })

  it('renders error', () => {
    useQuestionDataMock.mockReturnValue({
      error: true,
      question: undefined,
      loading: true
    })
    const { getByText } = render(<QuestionStats />)
    expect(getByText('There is something wrong with API')).toBeInTheDocument()
  })
})
