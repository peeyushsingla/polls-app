import React from 'react';
import { useParams } from 'react-router-dom';
import Spinner from '../../Components/Spinner/Spinner';
import useQuestionData from '../../Hooks/useQuestionData';
import SurveyStats from '../../Components/SurveyStats/SurveyStats';

const QuestionStats: React.FunctionComponent = () => {
  const { questionId } = useParams();
  const { error, question, loading } = useQuestionData(questionId);

  if(error){
    return (<p>There is something wrong with API</p>)
  }

  if (loading || !question) {
    return <Spinner />;
  }

  return <SurveyStats {...question} />;
};

export default QuestionStats;
