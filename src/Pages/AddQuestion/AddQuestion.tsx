import React, { useState, ChangeEvent, FormEvent } from 'react';
import * as uuid from 'uuid'
import { useHistory } from 'react-router-dom';
import { Form, Button, InputGroup } from 'react-bootstrap';
import ChoiceList from '../../Components/ChoiceList/ChoiceList';
import Spinner from '../../Components/Spinner/Spinner';
import { createQuestion } from '../../Api/client';
import { Choice } from './AddQuestion.d';

const AddQuestion: React.FunctionComponent = () => {
  const { push } = useHistory();
  const [choiceList, setChoiceList] = useState<Choice[]>([]);
  const [loading, setLoading] = useState(false);
  const [formData, setFormData] = useState({ question: '', choice: '' });

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const {target: { value, name }} = event;
    setFormData((oldFormData) => ({ ...oldFormData, ...{[name]: value}}))
  }

  const addChoice = () => {
    const newChoice = { id: uuid.v4(), text: formData.choice }
    setChoiceList((oldChoiceList) => [...oldChoiceList, newChoice])
    setFormData((oldFormData) => ({...oldFormData, ...{choice: ''}}))
  }

  const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const data = {
      question: formData.question,
      choices: choiceList.map((choice) => choice.text)
    }
    setLoading(true)
    const question = await createQuestion(data);
    setLoading(false)
    push(question.url);
  }

  const validForm = (choiceList.length > 1 && formData.question);

  if(loading){
    return <Spinner />
  }

  return (
    <Form onSubmit={ handleSubmit }>
      <Form.Group>
        <Form.Label>Question</Form.Label>
        <Form.Control data-testid='question' name='question' type="text" placeholder="Enter Question" onChange={handleChange} value={formData.question}/>
      </Form.Group>

      <Form.Group>
        <Form.Label>Choice</Form.Label>
        <InputGroup className="mb-3">
          <Form.Control data-testid='choice' name='choice' type="text" placeholder="Enter choice" onChange={handleChange} value={formData.choice} />
          <InputGroup.Append>
            <Button variant="outline-secondary" disabled={!formData.choice.replace(/\s/g, '')} onClick={addChoice}>Add Choice</Button>
          </InputGroup.Append>
        </InputGroup>
      </Form.Group>
      <ChoiceList choiceList={choiceList} />
      <Button variant="primary" type="submit" disabled={!validForm}>
        Submit
      </Button>
    </Form>
  );
}

export default AddQuestion;
