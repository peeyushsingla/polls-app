import React from 'react';
import { render } from '@testing-library/react';
import AddQuestion from './AddQuestion';

const mockedPush = jest.fn();
jest.mock('react-router-dom', () => ({
  useHistory: () => ({ push: mockedPush })
}));

jest.mock('../../Api/client');

describe(('QuestionDetails'), () => {
  afterEach(() => {
    jest.clearAllMocks();
  })

  it('renders create question form', () => {
    const { asFragment } = render(<AddQuestion />)
    expect(asFragment()).toMatchSnapshot()
  })
})
