import React from 'react';
import QuestionDetails from './QuestionDetails';
import { render, fireEvent } from '@testing-library/react';
import useQuestionData from '../../Hooks/useQuestionData';
import { submitSurvey } from '../../Api/client'
import { questionMock } from '../../Api/client.mocks';

jest.mock('../../Hooks/useQuestionData');
const useQuestionDataMock = useQuestionData as jest.Mock;

const mockedPush = jest.fn();
jest.mock('react-router-dom', () => ({
  useParams: () => ({ questionId: 'questionId' }),
  useHistory: () => ({ push: mockedPush })
}));

jest.mock('../../Api/client');
const submitSurveyMock = submitSurvey as jest.Mock;

describe(('QuestionDetails'), () => {
  afterEach(() => {
    jest.clearAllMocks();
  })

  it('renders survey form', () => {
    useQuestionDataMock.mockReturnValue({
      error: false,
      question: questionMock,
      loading: false
    })
    const { asFragment } = render(<QuestionDetails />)
    expect(asFragment()).toMatchSnapshot()
  })

  it('submits survey on clicking submit', () => {
    useQuestionDataMock.mockReturnValue({
      error: false,
      question: questionMock,
      loading: false
    })
    const { getByText } = render(<QuestionDetails />)
    fireEvent.click(getByText('Choice1'))
    fireEvent.click(getByText('Submit'))

    expect(submitSurveyMock).toBeCalledWith('/questions/1/choices/67');
  })

  it('renders loading', () => {
    useQuestionDataMock.mockReturnValue({
      error: false,
      question: undefined,
      loading: true
    })
    const { getByTestId } = render(<QuestionDetails />)
    expect(getByTestId('spinner')).toBeInTheDocument()
  })

  it('renders error', () => {
    useQuestionDataMock.mockReturnValue({
      error: true,
      question: undefined,
      loading: true
    })
    const { getByText } = render(<QuestionDetails />)
    expect(getByText('There is something wrong with API')).toBeInTheDocument()
  })
})
