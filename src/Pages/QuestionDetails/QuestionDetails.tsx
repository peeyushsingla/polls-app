import React from 'react';
import { useParams, useHistory } from 'react-router-dom'
import Spinner from '../../Components/Spinner/Spinner'
import { submitSurvey } from '../../Api/client'
import useQuestionData from '../../Hooks/useQuestionData';
import SurveyForm from '../../Components/SurveyForm/SurveyForm';

const QuestionDetails: React.FunctionComponent = () => {
  const { questionId } = useParams();
  const { push } = useHistory();
  const { question, loading, error } = useQuestionData(questionId);

  const onSubmit = async (selectedChoiceUrl: string) => {
    await submitSurvey(selectedChoiceUrl)
    push(`/questions/${questionId}/stats`)
  }

  if(error){
    return(<p>There is something wrong with API</p>)
  }

  if (loading || !question) {
    return (<Spinner />)
  }

  return (
    <SurveyForm question={question} onSubmit={onSubmit}/>
  );
}

export default QuestionDetails;
