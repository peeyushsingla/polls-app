import React from 'react';
import QuestionCard from '../../Components/QuestionCard/QuestionCard';
import Spinner from '../../Components/Spinner/Spinner'
import { Container } from './QuestionList.styled';
import useQuestionsListData from '../../Hooks/useQuestionsListData';

const QuestionList: React.FunctionComponent = () => {
  const { questions, loading, error} = useQuestionsListData()

  if(error){
    return (<p>There is something wrong with API</p>)
  }

  if (loading || !questions){
    return (<Spinner />)
  }

  const questionsCards = questions.map((question) => {
    return <QuestionCard key={question.url} {...question} />
  })
  return (<Container>{questionsCards}</Container>);
}

export default QuestionList;
