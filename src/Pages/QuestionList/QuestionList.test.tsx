import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { render, } from '@testing-library/react';
import useQuestionsListData from '../../Hooks/useQuestionsListData';
import QuestionList from './QuestionList';
import { questionListMock } from '../../Api/client.mocks';

jest.mock('../../Hooks/useQuestionsListData');
const useQuestionsListDataMock = useQuestionsListData as jest.Mock;

describe(('QuestionList'), () => {
  afterEach(() => {
    jest.clearAllMocks();
  })

  const setup = () => render(
    <MemoryRouter><QuestionList /></MemoryRouter>
  )

  it('renders questionList', () => {
    useQuestionsListDataMock.mockReturnValue({
      error: false,
      questions: questionListMock,
      loading: false
    })
    const { asFragment } = setup();
    expect(asFragment()).toMatchSnapshot()
  })

  it('renders loading', () => {
    useQuestionsListDataMock.mockReturnValue({
      error: false,
      questions: undefined,
      loading: true
    })
    const { getByTestId } = setup();
    expect(getByTestId('spinner')).toBeInTheDocument()
  })

  it('renders error', () => {
    useQuestionsListDataMock.mockReturnValue({
      error: true,
      question: undefined,
      loading: true
    })
    const { getByText } = setup();
    expect(getByText('There is something wrong with API')).toBeInTheDocument();
  })
})
