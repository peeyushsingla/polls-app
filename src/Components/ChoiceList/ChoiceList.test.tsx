import React from 'react';
import { render } from '@testing-library/react';
import * as uuid from 'uuid';
import ChoiceList from './ChoiceList';

describe('ChoiceList', () => {
  const mockedChoiceList = [
    {
      id: uuid.v4(),
      text: 'Lorem ipsum dolor sit amet',
    },
    {
      id: uuid.v4(),
      text: 'Lorem, ipsum dolor.',
    }
  ]

  it('renders ChoiceList', () => {
    const { asFragment } = render(
      <ChoiceList choiceList={mockedChoiceList} />
    );
    expect(asFragment()).toMatchSnapshot()
  })

  it('renders message to add choices', () => {
    const { getByText } = render(
      <ChoiceList choiceList={[]} />
    );
    expect(getByText('Please add Choices')).toBeInTheDocument()
  })
})
