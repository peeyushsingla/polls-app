import React from 'react';
import { Choice } from '../../Pages/AddQuestion/AddQuestion.d'

const ChoiceList: React.FunctionComponent<{ choiceList: Choice[] }> = ({ choiceList }) => {
  if (!choiceList.length) {
    return <p>Please add Choices</p>
  }

  const choiceRows = choiceList.map((choice) => {
    return <li key={choice.id}>{choice.text}</li>
  })

  return (
    <>
      <h5>Choices</h5>
      <ul>{ choiceRows }</ul>
    </>
  )
}

export default ChoiceList;
