import styled from 'styled-components';
import { Navbar } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export const StyledNavbar = styled(Navbar)`
  margin-bottom: 1rem;
`;

export const NavLink = styled(Link)`
  color: white !important;
  text-decoration: none !important;
`;
