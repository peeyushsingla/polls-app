import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { render } from '@testing-library/react';
import Header from './Header';

describe('Header', () => {
  it('renders header', () => {
    const { asFragment } = render(<MemoryRouter><Header /></MemoryRouter>);
    expect(asFragment()).toMatchSnapshot()
  })
})
