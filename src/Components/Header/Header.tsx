import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { StyledNavbar, NavLink } from './Header.styled';

const Header: React.FunctionComponent = () =>
  <StyledNavbar bg="dark" variant="dark">
  <Link to='/'><Navbar.Brand>Questions</Navbar.Brand></Link>
    <Nav className="ml-auto">
      <Nav.Item>
        <NavLink to="/questions/new">Add Question</NavLink>
      </Nav.Item>
    </Nav>
  </StyledNavbar>

export default Header;
