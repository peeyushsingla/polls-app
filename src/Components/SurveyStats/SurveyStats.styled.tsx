import styled from 'styled-components';

export const StyledContainer = styled.div`
  h5 {
    font-size: 1.25rem;
    margin-bottom: 1rem;
  }
`;
