import React from 'react';
import { render } from '@testing-library/react';
import SurveyStats from './SurveyStats';
import { questionMock } from '../../Api/client.mocks';

describe('SurveyStats', () => {
  it('renders SurveyStats table', () => {
    const { asFragment } = render(<SurveyStats {...questionMock} />);
    expect(asFragment()).toMatchSnapshot()
  })
})
