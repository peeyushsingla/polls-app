import React from 'react';
import { Table, ProgressBar } from 'react-bootstrap';
import { getPercentage } from '../../Util/numberUtil';
import { StyledContainer } from './SurveyStats.styled';
import { Question } from '../../Api/client.d';

const SurveyStats: React.FunctionComponent<Question> = ({ question, choices }) => {
  const totalVotes = choices.reduce((sum, { votes }) => votes + sum, 0);

  const choiceRows = choices.map(({ url, choice, votes }) =>
    <tr key={url}>
      <td>{choice}</td>
      <td>{votes}</td>
      <td><ProgressBar now={getPercentage(votes, totalVotes)} /></td>
    </tr>
  );

  return (
    <StyledContainer>
      <h5>Question: {question}</h5>
      <Table bordered hover>
        <thead>
          <tr>
            <th>Choice</th>
            <th>Votes</th>
            <th>Percentage</th>
          </tr>
        </thead>
        <tbody>
          {choiceRows}
        </tbody>
      </Table>
    </StyledContainer>
  );
}

export default SurveyStats;
