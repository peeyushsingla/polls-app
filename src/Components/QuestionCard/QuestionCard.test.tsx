import React from 'react';
import { render } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import QuestionCard from './QuestionCard';
import { questionMock } from '../../Api/client.mocks';

describe('QuestionCard', () => {
  it('renders questions card', () => {
    const { asFragment } = render(
      <MemoryRouter>
        <QuestionCard {...questionMock} />
      </MemoryRouter>
    );
    expect(asFragment()).toMatchSnapshot()
  })
})
