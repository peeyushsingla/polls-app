import styled from 'styled-components';
import { Card } from 'react-bootstrap';

export const StyledCard = styled(Card)`
  width: 15rem;
  min-height: 12rem;
  margin: 1rem;
  cursor: pointer;
  color: white;
  &:hover {
    box-shadow: 0 10px 20px 0 rgba(0, 0, 0, 0.2), 0 10px 20px 0 rgba(0, 0, 0, 0.19);
  }
  a {
    text-decoration: none !important;
    color: inherit !important;
    height: 100%;
  }
`;
