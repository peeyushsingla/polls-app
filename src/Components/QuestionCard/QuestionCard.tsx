import * as React from 'react';
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { StyledCard } from './QuestionCard.styled';
import { formatDate } from '../../Util/dateUtil';
import { Question } from '../../Api/client.d';

const QuestionCard: React.SFC<Question> = ({ question, url, published_at,  choices}) =>
  <StyledCard bg='secondary'>
    <Link to={url}>
      <Card.Body>
        <Card.Title>{question}</Card.Title>
        <Card.Text>{formatDate(published_at)}</Card.Text>
        <Card.Text>Choices: {choices.length}</Card.Text>
      </Card.Body>
    </Link>
  </StyledCard>

export default QuestionCard;
