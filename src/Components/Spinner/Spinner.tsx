import React from 'react';
import { Spinner as BootstrapSpinner } from 'react-bootstrap';
import { StyledContainer } from './Spinner.styled';

const Spinner: React.FunctionComponent = () =>
  <StyledContainer>
    <BootstrapSpinner animation="border" data-testid='spinner' />
  </StyledContainer>

export default Spinner;
