import { Question } from '../../Api/client.d';

export interface SurveyFormProps {
  question: Question;
  onSubmit: (selectedOption: string) => void;
}
