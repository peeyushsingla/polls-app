import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import SurveyForm from './SurveyForm';
import { questionMock } from '../../Api/client.mocks';

describe('SurveyForm', () => {
  const surveyFormProps = {
    question: questionMock,
    onSubmit: jest.fn()
  }

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('renders surveyForm', () => {
    const { asFragment } = render(<SurveyForm {...surveyFormProps} />);
    expect(asFragment()).toMatchSnapshot()
  })

  it('calls onSubmit when button clicked', () => {
    const { getByText } = render(<SurveyForm {...surveyFormProps} />);
    //Without selecting choice form is disabled
    expect(getByText('Submit')).toBeDisabled()

    fireEvent.click(getByText('Choice1'))
    fireEvent.click(getByText('Submit'))

    expect(surveyFormProps.onSubmit).toBeCalled();
  })
})
