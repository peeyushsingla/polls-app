import React, { useState, ChangeEvent, FormEvent } from 'react';
import { Form, Card, Button } from 'react-bootstrap';
import RadioButton from '../RadioButton/RadioButton';
import { SurveyFormProps } from './SurveyForm.d';

const SurveyForm: React.FunctionComponent<SurveyFormProps> = ({
  question: { choices, question },
  onSubmit
}) => {
  const [selectedChoice, setSelectedChoice] = useState<string>('')

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setSelectedChoice(e.target.value);
  }

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    onSubmit(selectedChoice);
  }

  const choiceCheckbox = choices.map((choice) => {
    return <RadioButton key={choice.url} choice={choice} onChange={handleChange} />
  })

  return(
    <Card>
      <Card.Body>
        <Card.Title>Question: {question}</Card.Title>
        <Form onSubmit={handleSubmit}>
          <Form.Group>
            {choiceCheckbox}
          </Form.Group>
          <Button variant="primary" type="submit" disabled={!selectedChoice}>
            Submit
          </Button>
        </Form>
      </Card.Body>
    </Card>
  )
}


export default SurveyForm;
