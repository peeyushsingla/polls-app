import React from 'react';
import { Form } from 'react-bootstrap';
import { RadioButtonProps } from './RadioButton.d';

const RadioButton: React.FunctionComponent<RadioButtonProps> = ({
  choice: { choice, url },
  onChange
}) => <Form.Check label={choice} type='radio' name="poll-options" id={choice} value={url} onChange={onChange}/>

export default RadioButton;
