import { Choice } from '../../Api/client';

export interface RadioButtonProps {
  choice: Choice;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
}
