import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import RadioButton from './RadioButton';

describe('RadioButton', () => {
  const radioButtonProps = {
    choice: {
      choice: 'Choice1',
      votes: 1,
      url: '/questions/22/choices/67',
    },
    onChange: jest.fn()
  }

  afterEach(() => {
    jest.clearAllMocks();
  })

  it('renders radio button', () => {
    const { asFragment } = render(
      <RadioButton {...radioButtonProps} />
    );
    expect(asFragment()).toMatchSnapshot()
  })

  it('calls onChange on triggering click', () => {
    const { getByText } = render(
      <RadioButton {...radioButtonProps} />
    );
    expect(getByText('Choice1')).toBeInTheDocument()
    fireEvent.click(getByText('Choice1'))
    expect(radioButtonProps.onChange).toBeCalled();
  })
})
