export interface Choice {
  choice: string;
  votes: number;
  url: string
}

export type QuestionsList = Question[];

export interface Question {
  question: string;
  published_at: string;
  url: string;
  choices: Choice[]
}

export interface NewQuestion {
  question: string;
  choices: string[];
}
