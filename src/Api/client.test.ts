import moxios from 'moxios';
import { getQuestionsList, getQuestion, submitSurvey, createQuestion, API_URL } from './client';
import {
  newQuestionMock,
  questionListMock,
  questionMock,
  surveySubmissionSuccessMock,
} from './client.mocks';

describe('Api client', () => {
  beforeEach(function () {
    moxios.install();
  });

  afterEach(function () {
    moxios.uninstall();
    jest.clearAllMocks();
  });

  it('fetches question list from Api', async () => {
    moxios.stubRequest(`${API_URL}/questions`, {
      status: 200,
      response: questionListMock
    });

    const questionList = await getQuestionsList();
    expect(questionList).toEqual(questionListMock);
  });

  it('fetches question details from API', async () => {
    moxios.stubRequest(`${API_URL}/questions/1`, {
      status: 200,
      response: questionMock
    });

    const question = await getQuestion('1');
    expect(question).toEqual(questionMock);
  });

  it('posts survey', async () => {
    moxios.stubRequest(`${API_URL}/questions/1/choices/1`, {
      status: 200,
      response: surveySubmissionSuccessMock
    });

    const response = await submitSurvey('/questions/1/choices/1')
    expect(response).toEqual(surveySubmissionSuccessMock);
  });

  it('submits new question', async () => {
    moxios.stubRequest(`${API_URL}/questions`, {
      status: 200,
      response: questionMock,
    });

    const response = await createQuestion(newQuestionMock);
    expect(response).toEqual(questionMock);
  });
})
