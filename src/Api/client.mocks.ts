import { Question, QuestionsList, NewQuestion } from './client.d';

export const questionMock: Question = {
  question: 'Question Value?',
  published_at: '2020-06-12T15:33:50.103235+00:00',
  url: '/questions/1',
  choices: [
    {
      choice: 'Choice1',
      votes: 1,
      url: '/questions/1/choices/67',
    },
    {
      choice: 'Choice2',
      votes: 1,
      url: '/questions/1/choices/69',
    }
  ],
};

export const surveySubmissionSuccessMock = {
  choice: 'Answer 2',
  votes: 4,
  url: '/questions/21/choices/66',
};

export const questionListMock: QuestionsList = [
  {
    question: 'Question Value?',
    published_at: '2020-06-12T15:33:50.103235+00:00',
    url: '/questions/1',
    choices: [
      {
        choice: 'Q1Choice1',
        votes: 1,
        url: '/questions/1/choices/67',
      },
      {
        choice: 'Q1Choice2',
        votes: 1,
        url: '/questions/1/choices/69',
      },
    ],
  },
  {
    question: 'Question Value 2?',
    published_at: '2020-06-12T15:33:50.103235+00:00',
    url: '/questions/2',
    choices: [
      {
        choice: 'Q2Choice1',
        votes: 1,
        url: '/questions/2/choices/67',
      },
      {
        choice: 'Q2Choice2',
        votes: 1,
        url: '/questions/2/choices/69',
      },
    ],
  },
  {
    question: 'Question Value 3?',
    published_at: '2020-06-12T15:33:50.103235+00:00',
    url: '/questions/3',
    choices: [
      {
        choice: 'Q3Choice1',
        votes: 1,
        url: '/questions/3/choices/67',
      },
      {
        choice: 'Q3Choice2',
        votes: 1,
        url: '/questions/3/choices/69',
      },
    ],
  },
];

export const newQuestionMock: NewQuestion = {
  question: 'Question Value?',
  choices: ['Choice1', 'Choice2'],
};
