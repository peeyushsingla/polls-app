import axios from 'axios';
import { QuestionsList, Question, NewQuestion } from './client.d';

export const API_URL = 'https://polls.apiblueprint.org';

export const getQuestionsList = async () => {
  const { data } = await axios.get<QuestionsList>(`${API_URL}/questions`);
  return data;
};

export const getQuestion = async (id: string) => {
  const { data } = await axios.get<Question>(`${API_URL}/questions/${id}`);
  return data;
};

export const submitSurvey = async (choiceUrl: string) => {
 const { data } = await axios.post(`${API_URL}${choiceUrl}`);
  return data;
};

export const createQuestion = async (question: NewQuestion) => {
  const { data } = await axios.post<Question>(`${API_URL}/questions`, {
    ...question
  });
  return data;
};
