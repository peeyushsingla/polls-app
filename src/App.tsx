import React from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import Header from './Components/Header/Header'
import QuestionList from './Pages/QuestionList/QuestionList';
import QuestionDetails from './Pages/QuestionDetails/QuestionDetails';
import QuestionStats from './Pages/QuestionStats/QuestionStats';
import AddQuestion from './Pages/AddQuestion/AddQuestion';

function App() {
  return (
    <div className="App">
      <Router>
        <Header />
        <Container>
          <Switch>
            <Route exact path='/'>
              <QuestionList />
            </Route>
            <Route exact path='/questions/new'>
              <AddQuestion />
            </Route>
            <Route exact path='/questions/:questionId'>
              <QuestionDetails />
            </Route>
            <Route exact path='/questions/:questionId/stats'>
              <QuestionStats />
            </Route>
            <Redirect to="/" />
          </Switch>
        </Container>
      </Router>
    </div>
  );
}

export default App;
