import { formatDate } from './dateUtil';

describe('dateUtil', () => {
  it('format date to readable format', () => {
    expect(formatDate("2020-06-12T15:33:50.103235+00:00")).toEqual("12/Jun/2020")
  })
})
