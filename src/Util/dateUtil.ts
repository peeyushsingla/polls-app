export const formatDate = (date: string) => {
  const parsedDate = new Date(date);
  const dateTimeFormat = new Intl.DateTimeFormat('en', {
    year: 'numeric',
    month: 'short',
    day: '2-digit',
  });

  const [{ value: month },,{ value: day },,{ value: year }] = dateTimeFormat.formatToParts(parsedDate)
  return(`${day}/${month}/${year}`);
}
