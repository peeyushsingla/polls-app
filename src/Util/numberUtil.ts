export const getPercentage = (value: number, total: number) => {
  if (value === 0 || total === 0){
    return 0
  }

  return Math.floor((value / total) * 100);
}
