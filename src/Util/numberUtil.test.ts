import { getPercentage } from './numberUtil';

describe('numberUtil', () => {
  it('returns 0 if total or value is zero', () => {
    expect(getPercentage(1, 0)).toEqual(0);
    expect(getPercentage(0, 1)).toEqual(0);
  });

  it('returns rounded percentage', () => {
    expect(getPercentage(10, 30)).toEqual(33);
    expect(getPercentage(50, 500)).toEqual(10);
  });
});
