## Survey App

### Main dependencies
* [react-bootstrap](https://react-bootstrap.netlify.app/)
* [react-testing-library](https://testing-library.com/docs/intro)
* [styled-components](https://styled-components.com/)
* TypeScript
* [Axios](https://github.com/axios/axios)

### Setup

* Git clone repo `git clone git@gitlab.com:peeyushsingla/polls-app.git`
* Install dependencies `cd polls-app && yarn`
* Start server `yarn start`
* Run tests `yarn test`

### API
* [Documentation](http://docs.pollsapi.apiary.io/)

### Tasks Accomplished
* [List of questions page](https://pasteboard.co/8AReDrPvo.jpg)
* [Question detail page](https://pasteboard.co/8ARC7212L.jpg)
* Create new question page.

### Coverage:-
* 85% of the code is having test coverage.<br/>
![alt text](public/coverage.png)


### Notes for reviewer:-
* Submitting a poll and getting the fresh data for that poll takes time e.g if you select some option from the poll, it won't be displayed right away on stats(it appears backend has some sort of delay). It appears in like 2-3 seconds so you might need to reload.
